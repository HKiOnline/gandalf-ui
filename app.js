window.onload = function () {

  var app = new Vue ({
      el: "#app",
      data: {
        quote: {id: '', quote: '', author: '' }
      },

      created: function() {
        this.getQuote();
      },

      methods: {
        getQuote: function() {

          console.log("Contacting the API")

          $.ajax({
            context: this,
            url: "http://localhost:8090/api",
            success: function(result) {
              this.quote = result;
          }
          })
        }
      }

  });


};
